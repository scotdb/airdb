# AirDB

Aircraft spotter's database repository, set up according to standard pattern for Db2 database deployments.

## Structure or repository

-- Baseline : contains a baseline build (will create current production database in one pass)
-- Deltas   : contains incremental builds
   -- air-\<ccyymmdd\>-\<description\> : each subdirectory contains one incremental build stage
