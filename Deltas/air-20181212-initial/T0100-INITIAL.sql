INSERT INTO DBAIR001.T0100USER
(
FIRST_NAME                      ,    
SURNAME                         ,   
COUNTRY_CODE                    ,   
SECURITY_ACCESS_LEVEL           ,
USERNAME                        ,    
USER_PASSWORD                   ,   
LAST_MODIFIED_BY                ,  
LAST_MODIFIED_TIMESTAMP         , 
ENTERED_BY                      
)
VALUES
(
 'System','Administrator',
 'GB',
 0,
 'sysadmin',
 'sysadmin',
 1,
 current timestamp,
 1
)
;
