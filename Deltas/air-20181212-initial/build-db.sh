#!/usr/bin/ksh93

#
# build-db.sh
# =============
# Build script for a CLPPlus deployment
#
# Parameters
# ----------
#  1 : Connection location (LUW database)
#  2 : Connection hostname
#  3 : Connection port number
#  4 : Connection ID
#  5 : Connection PW
#  6 : Local object SQLID
#
# Notes
# -----
#

# Check that $DB2_HOME is set
if [ -z "$DB2_HOME" ]
then
  echo "Run db2profile to set up DB2 environment"
  exit 8;
else
  echo "DB2 environment set correctly"
fi

#
# Check for inputs
#
if [ $# = 8 ]; then
# if [ $# = 8 ]; then
  echo "Six arguments passed";
  # echo "Eight arguments passed";
  location="$1";
  host="$2";
  portnum="$3";
  userid="$4";
  pword="$5";
  sqlid="$6";
else
  #
  # If no inputs then prompt
  #
  echo "Unexpected number of arguments passed : prompting for data";
  print -n "Connection database (LUW) or location (z/OS): ";read location;
  print -n "Enter hostname: ";read host;
  print -n "Enter port number: "; read portnum;
  print -n "Enter connection user ID: "; read userid;
  # Do not echo password to screen
  stty -echo;print -n "Enter connection password: "; read pword;stty echo;printf "\n";
  print -n "Enter current SQLID: "; read sqlid;
fi

# Check operating system
os=`uname`;
echo "Running on $os";

# Make various details upper case
location=$(echo $location|tr "[:lower:]" "[:upper:]");
sqlid=$(echo $sqlid|tr "[:lower:]" "[:upper:]");

#
# Check database / location name
#
if echo $location | egrep -q '^D(A|B|D|P|T).{3}[0-9]{3}$'
then
  echo "Database name good";
else
  echo "Bad database name : standard is D(A|D|T|P)<eee><nnn>";exit 8;
fi

# Check port number is five digits
if echo $portnum | egrep -q '^[0-9]{5}$'
then
  echo "Port number good";
else
  echo "Bad port number";
  exit 8;
fi

#
# Check schema (SQLID)
#
if echo $sqlid | egrep -q '^DB.{3}[0-9]{3}$'
then
  echo "Schema (SQLID) good";
else
  echo "Bad schema (SQLID): standard is DB<eee><nnn>";exit 8;
fi

# Set local schema name to current SQLID
schema=$sqlid;
echo $schema;

#
# Set environment variable for use in CLP*PLUS
#
export DB2SQLID=$sqlid;

#
# Disable debug mode for SP deployments (z/OS only)
#
debug='DISABLE'
# export DB2DEBUG=$debug; 

#
# Now prepare to use CLPPlus
#
export DB2DSDRIVER_CFG_PATH=`pwd`;

#
# Use SSL connectivity for DB2 for LUW
#
# cp $HOME/security/key_$host.arm db2server.arm
# cp db2dsdriver.cfg.template.tls db2dsdriver.cfg
cp db2dsdriver.cfg.template.notls db2dsdriver.cfg


sed -i 's/$DBALIAS/'$location'/g' db2dsdriver.cfg
sed -i 's/$LOCATION/'$location'/g' db2dsdriver.cfg
sed -i 's/$DBHOST/'$host'/g' db2dsdriver.cfg
sed -i 's/$DBPORT/'$portnum'/g' db2dsdriver.cfg
sed -i 's/$USERID/'$userid'/g' db2dsdriver.cfg
sed -i 's/$PASSWORD/'$pword'/g' db2dsdriver.cfg

#
# Now we use CPP to produce SQL files
#
# cpp -D DB2TYPE=0 -P -w -I . dig_create_nicknames.cpp  dig_create_nicknames.sql

#
# Now use sed to swap in appropriate database and storage group names for z/OS
#

# sed -i 's/$FEDSCHEMA/'$fedschema'/g' dig_create_nicknames.sql

clpplus -nw $userid@$location @deploy.clpplus
# clpplus -nw $userid@$location @deploy.clp
# clpplus -nw $USER/$pword@localhost:$portnum/$location @basedb.clp

# rm db2server.arm
rm db2dsdriver.cfg

# rm dig_create_nicknames.sql
