CREATE OR REPLACE VIEW DBAIR001.VSTATISTICS
(TOTAL_AIRCRAFT
,TOTAL_AIRPORTS
,TOTAL_VISITS
,TOTAL_FLIGHTS) AS
VALUES (
(SELECT COUNT(*) FROM DBAIR001.T0030AIRCRAFT),
(SELECT COUNT(*) FROM DBAIR001.T0010AIRPORT),                           
(SELECT COUNT(*) FROM DBAIR001.T0020AIRPORT_VISIT),
(0)
);


