INSERT INTO DBAIR001.T0090SECURITY_ACCESS_LEVEL
(SECURITY_ACCESS_LEVEL_ID 
,SECURITY_ACCESS_LEVEL_DESC)
VALUES
(  0,'Sysadmin'), 
(  1,'Staff'),                       
(  2,'Viewer')              
;
