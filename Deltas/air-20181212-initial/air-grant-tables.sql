--
-- Grant access to tables in AIR database
--
GRANT SELECT,INSERT,UPDATE,DELETE ON DBAIR001.T0010AIRPORT               TO GROUP AIRE01;  
GRANT SELECT,INSERT,UPDATE,DELETE ON DBAIR001.T0020AIRPORT_VISIT         TO GROUP AIRE01;  
