CREATE TABLE DBAIR001.T0090SECURITY_ACCESS_LEVEL
(SECURITY_ACCESS_LEVEL_ID SMALLINT NOT NULL
,SECURITY_ACCESS_LEVEL_DESC VARCHAR(50) NOT NULL
,CONSTRAINT PAIR002 PRIMARY KEY (SECURITY_ACCESS_LEVEL_ID)    
) IN TS_DBAIR001_8K
COMPRESS YES
;

-- Current values and counts (and potential details) 
--
-- SECURITY_ACCESS_LEVEL       COUNT          
-- --------------------- -----------
--                    -1           1
--                     0           1
--                     3          16 : management team
--                     4           3
--                     6          48 : regular staff
--                     7         274 : temps
--                   100           7 : external customers
--
