------------------------------------------------
-- DDL Statements for table "DBAIR001"."T0080COUNTRY"
------------------------------------------------
 
CREATE TABLE DBAIR001.T0080COUNTRY
( COUNTRY_CODE CHAR(2)     NOT NULL ,
  COUNTRY_NAME VARCHAR(50) NOT NULL ,
  CONSTRAINT PAIR008 PRIMARY KEY (COUNTRY_CODE)
) IN TS_DBAIR001_8K
COMPRESS YES
;
