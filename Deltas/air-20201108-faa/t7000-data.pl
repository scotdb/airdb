#!/usr/bin/perl

use warnings;
use strict;

my $filename = $ARGV[0];

open(FH, '<', $filename) or die $!;

while (<FH>) {
    my @data = split(',',$_);
    # Aircraft code is not nullable
    print $data[0];
    # Rest of data is nullable
    for my $i ( 1 .. ($#data - 1) ) {
       print $data[$i];
       if ($data[$i] =~ /^ *$/) {
         print 'Y';
       } else {
         print ' ';    
       }
    }     
    print "\n";
}

close(FH);