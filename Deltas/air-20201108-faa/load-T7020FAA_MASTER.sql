call SYSPROC.ADMIN_CMD('load from /tmp/MASTER.txt of del
  MODIFIED BY  DATEFORMAT="YYYYMMDD" NOCHARDEL
  MESSAGES ON SERVER
  replace into DBAIR001.T7020FAA_MASTER   
(N_NUMBER                    
,SERIAL_NUMBER               
,AIRCRAFT_TYPE_CODE          
,ENGINE_TYPE_CODE            
,YEAR_MANUFACTURED           
,TYPE_REGISTRANT             
,REGISTRANT_NAME             
,STREET1                     
,STREET2                     
,REGISTRANT_CITY             
,REGISTRANT_STATE           
,REGISTRANT_ZIPCODE          
,REGISTRANT_REGION           
,COUNTY_MAIL                 
,COUNTRY_MAIL                
,LAST_ACTIVITY_DATE          
,CERTIFICATE_ISSUE_DATE      
,CERTIFICATE_USES            
,TYPE_AIRCRAFT               
,TYPE_ENGINE                 
,STATUS_CODE                 
,MODE_S_CODE                 
,FRACTIONAL_OWNERSHIP        
,AIRWORTHINESS_DATE         
,OTHER_NAME_1               
,OTHER_NAME_2               
,OTHER_NAME_3               
,OTHER_NAME_4               
,OTHER_NAME_5               
,EXPIRATION_DATE               
,UNIQUE_ID                  
,KIT_MFR                    
,KIT_MODEL                  
,MODE_S_CODE_HEX            )
COPY YES TO /db2bck11/dbair001'
);
