
SELECT   
N_NUMBER, AIRCRAFT_TYPE, CN, OWNER, STATUS_CODE, AW_DATE,
AW_DATE - MAX(AW_DATE) OVER(ORDER BY AW_DATE ROWS BETWEEN 1 PRECEDING AND 1 PRECEDING) AS PREVIOUS_AW_DATE
-- OLAP expressions always go here 
FROM DBAIR001.VB737  
ORDER BY
PREVIOUS_AW_DATE DESC
-- you will need to change the sort order to display longest period first

-- OLAP Expression template
--
-- function(params) 
--   OVER(
--     PARTITION BY <cols>
--     ORDER BY <cols>
--     ROWS | RANGE BETWEEN <start> AND <end>)
--
--  where <start> and <end> can be -
--    (CURRENT ROW | (<n>|UNBOUNDED) (PRECEDING|FOLLOWING))
--
;
