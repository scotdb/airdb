WITH CTE1 AS (SELECT   
N_NUMBER, AIRCRAFT_TYPE, CN, OWNER, STATUS_CODE, AW_DATE,
length(N_NUMBER) AS LEN_N 
FROM  
DBAIR001.VB737  
ORDER BY
N_NUMBER ASC
),
CTE2 AS (
    SELECT N_NUMBER, AIRCRAFT_TYPE, CN, OWNER, STATUS_CODE, AW_DATE, LEN_N,
    CHAR(SUBSTR(N_NUMBER, LEN_N, 1)) AS FINAL_CHAR,
    CHAR(SUBSTR(N_NUMBER, LEN_N -1, 1)) AS PENULTIMATE_CHAR
    FROM CTE1
)
SELECT *, LEN_N - (CASE WHEN PENULTIMATE_CHAR BETWEEN '0' AND '9' THEN 2 
    WHEN FINAL_CHAR BETWEEN '0' AND '9' THEN 1
    ELSE 0
    END) AS NUM_CHARS
  FROM CTE2
  ORDER BY NUM_CHARS, N_NUMBER
--FETCH FIRST 100 ROWS ONLY

-- Functions needed -
--  LENGTH(column) : gives the length of the column
--  SUBSTR(column,start,for) : extracts part of string  
-- CHAR() : turns a number into a character - for comparison
-- Hints -
--   Determine how many characters are at the end of the N_NUMBER
--   Use that to derive the number of numeric digits
--   Use the number of digits in the sort order before the N_NUMBER itself
--
;
