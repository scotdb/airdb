CREATE TABLE DBAIR001.T0010AIRPORT
(AIRPORT_ID             INTEGER      NOT NULL GENERATED ALWAYS AS IDENTITY (
                    START WITH +1
                    INCREMENT BY +1
                    MINVALUE +1
                    MAXVALUE +2147483647
                    NO CYCLE
                    NO CACHE
                    NO ORDER ) 
,AIRPORT_NAME            VARCHAR(50)  NOT NULL  
,IATA_CODE               CHAR(3)                    
,ICAO_CODE               CHAR(4)
,CONSTRAINT PAIR001 PRIMARY KEY (AIRPORT_ID)   
) IN TS_DBAIR001_8K
COMPRESS YES;
