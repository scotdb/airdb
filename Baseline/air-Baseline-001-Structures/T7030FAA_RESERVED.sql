CREATE TABLE DBAIR001.T7030FAA_RESERVED
(N_NUMBER               CHAR(5) NOT NULL 
,REGISTRANT             CHAR(50) 
,STREET_ADDRESS         CHAR(33) 
,STREET2                CHAR(33) 
,CITY                   CHAR(18) 
,STATE                  CHAR(2) 
,ZIPCODE                CHAR(10) 
,RESERVE_DATE           DATE 
,TYPE_RESERVATION       CHAR(2) 
,EXPIRATION_NOTICE_DATE DATE 
,N_NUMBER_FOR_CHANGE    CHAR(5) 
,PURGE_DATE             DATE
,CONSTRAINT PAIR7030 PRIMARY KEY
 (N_NUMBER)
) IN TS_DBAIR001_8K
COMPRESS YES ADAPTIVE
; 
